const writeJsonObjectToJson = require("./writeJsonObjectToJson");
const fs = require('fs');
const csvParse = require('csv-parse');

function matchesPlayedPerYear() {

    let jsonObject = {};

    fs.createReadStream('src/data/matches.csv').pipe(csvParse()).on('data', (records) => {
        if (records[1] in jsonObject) {
            jsonObject[records[1]] += 1;
        }
        else {
            if (records[1] != 'season') {
                jsonObject[records[1]] = 1;
            }
        }

    }).on('end', function () {

        const filename = './src/public/output/matchesPerYear.json';

        writeJsonObjectToJson(filename, jsonObject);


    })

}

module.exports = matchesPlayedPerYear;