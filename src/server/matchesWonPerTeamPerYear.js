const writeJsonObjectToJson = require("./writeJsonObjectToJson");
const fs = require('fs');
const csvParse = require('csv-parse');

function matchesWonPerTeamPerYear() {

    let jsonObject = {};

    fs.createReadStream('src/data/matches.csv').pipe(csvParse()).on('data', (records) => {

        if (records[8] == "normal") {
            if (records[1] in jsonObject) {
                let getJsonObject = jsonObject[records[1]];
                if (records[10] in getJsonObject) {
                    getJsonObject[records[10]] = getJsonObject[records[10]] + 1;
                }
                else {
                    getJsonObject[records[10]] = 1;
                }
                jsonObject[records[1]] = getJsonObject;
            }
            else {
                let tempJsonObject = {};
                tempJsonObject[records[10]] = 1;
                jsonObject[records[1]] = tempJsonObject;
            }
        }


    }).on('end', function () {

        const filename = './src/public/output/matchesWonPerTeamPerYear.json';

        writeJsonObjectToJson(filename, jsonObject);

    });
}

module.exports = matchesWonPerTeamPerYear;