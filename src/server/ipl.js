const extraRunsPerTeam2016 = require("./extraRunsPerTeam2016");
const matchesPlayedPerYear = require("./matchesPlayedPerYear");
const matchesWonPerTeamPerYear = require("./matchesWonPerTeamPerYear");
const top10EconomicalBowlers2015 = require("./top10EconomicalBowlers2015");

matchesPlayedPerYear();
matchesWonPerTeamPerYear();
extraRunsPerTeam2016();
top10EconomicalBowlers2015();
