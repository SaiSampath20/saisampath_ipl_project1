const writeJsonObjectToJson = require("./writeJsonObjectToJson");
const fs = require('fs');
const csvParse = require('csv-parse');

function extraRunsPerTeam2016() {

    let matchId2016 = {};
    let jsonObject = {};

    fs.createReadStream('src/data/matches.csv').pipe(csvParse()).on('data', (matchesRow) => {

        if (matchesRow[1] == '2016') {
            matchId2016[matchesRow[0]] = 0;
        }

    }).on('end', () => {

        fs.createReadStream('src/data/deliveries.csv').pipe(csvParse()).on('data', (deliveryRow) => {

            if (deliveryRow[0] in matchId2016) {
                if (deliveryRow[3] in jsonObject) {
                    jsonObject[deliveryRow[3]] += parseInt(deliveryRow[16]);
                }
                else {
                    jsonObject[deliveryRow[3]] = parseInt(deliveryRow[16]);
                }

            }

        }).on('end', () => {

            const filename = './src/public/output/extraRunsPerTeam2016.json';

            writeJsonObjectToJson(filename, jsonObject);
        })

    })

}

module.exports = extraRunsPerTeam2016;