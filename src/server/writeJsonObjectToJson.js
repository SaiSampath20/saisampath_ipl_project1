const fs = require('fs');

function writeJsonObjectToJson(filename, jsonObject) {

    fs.writeFile(filename, JSON.stringify(jsonObject, null, 4), (err) => {
        if (err) {
            console.log('Error writing to json file', err);
        } else {
            console.log(`saved as ${filename}`);
        }
    });

}

module.exports = writeJsonObjectToJson;