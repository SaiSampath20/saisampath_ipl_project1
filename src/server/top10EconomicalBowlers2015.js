const fs = require('fs');
const csvParse = require('csv-parse');
const writeJsonObjectToJson = require('./writeJsonObjectToJson');

function top10EconomicalBowlers2015() {

    let jsonObject = {};
    let matchId2015 = {};

    fs.createReadStream('src/data/matches.csv').pipe(csvParse()).on('data', (matchesRow) => {
        if (matchesRow[1] == '2015') {
            matchId2015[matchesRow[0]] = 0;
        }
    }).on('end', () => {
        fs.createReadStream('src/data/deliveries.csv').pipe(csvParse()).on('data', (deliveryRow) => {

            if (deliveryRow[0] in matchId2015) {
                let runsPerBall = parseInt(deliveryRow[10]) + parseInt(deliveryRow[13]) + parseInt(deliveryRow[15]);

                if (deliveryRow[8] in jsonObject) {
                    jsonObject[deliveryRow[8]][0] += 1;
                    jsonObject[deliveryRow[8]][1] += runsPerBall;
                } else {
                    tempArray = [1, runsPerBall];
                    jsonObject[deliveryRow[8]] = tempArray;
                }
            }

        }).on('end', () => {

            let jsonArray = [];

            for (key in jsonObject) {
                let getArray = jsonObject[key];
                let numOvers = getArray[0] / 6;
                let economyRate = getArray[1] / numOvers;
                jsonArray.push([key, economyRate]);
            }

            jsonArray.sort(function (a, b) {
                return a[1] - b[1];
            });

            const top10Bowlers = jsonArray.slice(0, 10);
            const filename = './src/public/output/top10EconomicalBowlers2015.json';

            writeJsonObjectToJson(filename, top10Bowlers);
        })
    })

}

module.exports = top10EconomicalBowlers2015;